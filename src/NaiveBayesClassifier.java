import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

public class NaiveBayesClassifier {

	public static void main(String[] args) {
		new NaiveBayesClassifier();
	}

	public NaiveBayesClassifier(){
		MLE();
	}

	void MLE(){
		int[] count = new int[20];
		double[] probLabel = new double[20];
		int totalNumDocs = 0;
		FileInputStream fis = null;
		HashMap<Integer, Integer> mapDocLabels = new HashMap<>();
		ArrayList<String> docid = new ArrayList<>();
		ArrayList<String> doclblid = new ArrayList<>();
		int dID = 0;

		try {
			fis = new FileInputStream("train.label");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String strLine;

		//Read File Line By Line and store newgroup ids in doclblid arraylist
		try {
			while ((strLine = br.readLine()) != null)   {
				dID++;
				doclblid.add(strLine);
				docid.add(""+dID);

				switch(strLine){

				case "1": count[0]++;
				break;

				case "2": count[1]++;
				break;

				case "3": count[2]++;
				break;

				case "4": count[3]++;
				break;

				case "5": count[4]++;
				break;

				case "6": count[5]++;
				break;

				case "7": count[6]++;
				break;

				case "8": count[7]++;
				break;

				case "9": count[8]++;
				break;

				case "10": count[9]++;
				break;

				case "11": count[10]++;
				break;

				case "12": count[11]++;
				break;

				case "13": count[12]++;
				break;

				case "14": count[13]++;
				break;

				case "15": count[14]++;
				break;

				case "16": count[15]++;
				break;

				case "17": count[16]++;
				break;

				case "18": count[17]++;
				break;

				case "19": count[18]++;
				break;

				case "20": count[19]++;
				break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		//Close the input stream
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Please wait system is calculating the accuracy... ");
		for(int i =0; i<=19;i++){
			mapDocLabels.put(i,count[i]);
			totalNumDocs = totalNumDocs+count[i];
		}

		// calculating the probabilities for each newsgroup and store them in problabel array
		// P(Yk) = (number of docs labeled Yk)/total number of docs.
		for(int i = 0; i<=19;i++ ){
			probLabel[i] = (float)mapDocLabels.get(i)/(float)totalNumDocs;
			//						System.out.println(probLabel[i]);
		}

		// call MAP method. We are passing two parameters here. newsgroup ids and their probabilities.
		MAP(doclblid, probLabel);		
	}
	void MAP( ArrayList<String> doclblid, double[]  probLabel){

		FileInputStream fis = null;

		ArrayList<String> wrdid = new ArrayList<>();
		ArrayList<String> wrdDocID = new ArrayList<>();
		ArrayList<String> wrdcnt = new ArrayList<>();

		int[][] wrdLblMat = new int[20][61188];

		// Reading train.data file and store wordids, docids, wordcounts in arraylists
		try {
			fis = new FileInputStream("train.data");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String strLine;

		try {
			while ((strLine = br.readLine()) != null)   {
				String[] tempStore = new String[3];
				tempStore = strLine.split(" ");
				wrdDocID.add(tempStore[0]);
				wrdid.add(tempStore[1]);
				wrdcnt.add(tempStore[2]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Creating Matrix of label*words
		for(int i=0;i<wrdid.size();i++){
			String chkDocID = wrdDocID.get(i);
			String chkLblID = doclblid.get(Integer.parseInt(chkDocID)-1);
			String chkWrdID = wrdid.get(i);

			wrdLblMat[Integer.parseInt(chkLblID)-1][Integer.parseInt(chkWrdID)-1] = 
					wrdLblMat[Integer.parseInt(chkLblID)-1][Integer.parseInt(chkWrdID)-1]+Integer.parseInt(wrdcnt.get(i));
		}

		// Calculating Probability
		double[][] probMat = new double[20][61188];
		int[] rowSum = new int[20];

		// calculating row sums
		for(int i=0; i<20; i++){
			for (int j=0; j<61188;j++){
				rowSum[i] = rowSum[i]+wrdLblMat[i][j];
			}
		}

		//Calculating MAP result			P(Xi/Yk) = ((count of Xi in Yk)+(1/|V|))/(total number of words in Yk+1)
		for(int i=0; i<20;i++){
			for(int j=0; j<61188; j++){
				probMat[i][j] = ((double)wrdLblMat[i][j]+(0.00001634307))/((double)rowSum[i]+1);
				//				probMat[i][j] = ((double)wrdLblMat[i][j]+(0.9))/((double)rowSum[i]+(0.9*61188));
			}
		}

		// Question5  Top 100 words
		double[][] probNB = new double[20][61188];
		double[] sumNB = new double[61188];

		for(int p=0;p<61188;p++){							//P(Y)*P(X/Y)
			for(int q=0;q<20;q++){
				probNB[q][p] = probMat[q][p]*probLabel[q];
				sumNB[p] = sumNB[p]+probNB[q][p];
			}
		}

		double[][] normalNB = new double[20][61188];
		double[] finalSumNB = new double[61188];
		//Normalization
		for(int p=0;p<61188;p++){
			for(int q=0;q<20;q++){
				normalNB[q][p] = probNB[q][p]/sumNB[p];
				normalNB[q][p] = normalNB[q][p]*(Math.log(normalNB[q][p])/Math.log(2));
				finalSumNB[p] = finalSumNB[p]+normalNB[q][p];
			}
		}
		// store in treemap
		TreeMap<Double,Integer> tmap = new TreeMap<Double,Integer>();
		for( int i=0;i<61188;i++ ) {
			Object v = tmap.get(finalSumNB[i]);
			if(v==null){
				tmap.put( finalSumNB[i], i );
			}
		}
		// get sorted indexes using iterator
		int[] top100 = new int[100];
		int cn=0, tc=0;
		Iterator mapIterator = tmap.keySet().iterator();
		while (mapIterator .hasNext()) {  
			double key = (double) mapIterator.next();  
			int value = tmap.get(key);  
			//			System.out.println(key + " " + value);
			cn++;
			if(cn>23300){
				top100[tc] = value+1;
				tc++;
			}
		}

		//Reading Vocabulary file
		FileInputStream fis2 = null;
		try {
			fis2 = new FileInputStream("vocabulary.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ArrayList<String> vocab = new ArrayList<>();
		BufferedReader br2 = new BufferedReader(new InputStreamReader(fis2));
		String strLine2;
		try {
			while ((strLine2 = br2.readLine()) != null)   {
				vocab.add(strLine2);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Top 100 words are: ");
		for(int temp=99;temp>=0;temp--){
			System.out.print("'"+vocab.get(top100[temp]-1)+"', ");
		}
		// call classifier method with two parameters
		Classifier(probLabel, probMat);
	}

	void Classifier(double[] probLabel, double[][] probMat){

		FileInputStream fis = null;
		ArrayList<String> wrdid = new ArrayList<>();
		ArrayList<String> wrdDocID = new ArrayList<>();
		ArrayList<String> wrdcnt = new ArrayList<>();
		int[][] wrdDocMat = new int[7505][61188];
		double[][] transProbMat = new double[61188][20];

		double[][] matmul = new double[7505][20];
		double[][] matMLE = new double[7505][20];
		double[][] resultMat = new double[7505][20];
		// Read test.data file
		try {
			fis = new FileInputStream("test.data");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String strLine;
		try {
			while ((strLine = br.readLine()) != null)   {
				//System.out.println (strLine);
				String[] tempStore = new String[3];
				tempStore = strLine.split(" ");
				wrdDocID.add(tempStore[0]);
				wrdid.add(tempStore[1]);
				wrdcnt.add(tempStore[2]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// create word doc matrix. i.e.., test documents by total words matrix. 
		for(int i=0;i<wrdid.size();i++){
			String chkDocID = wrdDocID.get(i);
			String chkWrdID = wrdid.get(i);
			wrdDocMat[Integer.parseInt(chkDocID)-1][Integer.parseInt(chkWrdID)-1] = 
					wrdDocMat[Integer.parseInt(chkDocID)-1][Integer.parseInt(chkWrdID)-1]+Integer.parseInt(wrdcnt.get(i));
		}

		// Calculating the log of MAP result(log of probability matrix) and transposing it. i.e.., log2(P(Xi|Yk))
		for(int i=0;i<20;i++){
			for(int j=0;j<61188;j++){
				//				probMat[i][j] =  probMat[i][j] > 0 ? (float)(Math.log(probMat[i][j]) / Math.log(2)):0;
				probMat[i][j] = (double)(Math.log(probMat[i][j]) / Math.log(2));
				transProbMat[j][i]=probMat[i][j];
			}
		}

		//calculating log of MLE result. i.e.., log2(P(Yk)
		for(int i=0; i<probLabel.length; i++){
			probLabel[i] = (double)Math.log(probLabel[i])/Math.log(2);
		}

		// creating repeated matrix
		for(int i=0;i<7505;i++){
			for(int j=0; j<20; j++){
				matMLE[i][j] = probLabel[j];
			}
		}

		//Multiplication of matrices.   i.e.., sigmai (number of Xinew)log2(P(Xi|Yk))
		for(int i = 0;i < 7505;i++){
			for(int j = 0;j < 20;j++){
				for(int k = 0;k < 61188;k++){
					matmul[i][j] += (double)wrdDocMat[i][k] * transProbMat[k][j];
				}
			}
		}

		// Addition of matMul and matMLE matrices.   i.e.., log2(P(Yk))+ sigmai(number of Xinew)log2(P(Xi|Yk))
		for (int r = 0; r < 7505; r++) {
			for (int c = 0; c < 20; c++) {
				resultMat[r][c] = matmul[r][c] + matMLE[r][c];
			}
		}

		// get max probability
		int[] maxResultProbs = new int[7505];
		for ( int row=0; row < 7505; row++){
			double max = resultMat[row][0];
			maxResultProbs[row] = 1;
			for(int col = 0; col < 20; col++) {
				if ( resultMat[row][col] > max ){
					max = resultMat[row][col];
					maxResultProbs[row] = col+1;
				}
			}
		}
		// Reading test.label file
		FileInputStream fisl = null;
		int[] testlabels = new int[7505];
		try {
			fisl = new FileInputStream("test.label");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		BufferedReader brl = new BufferedReader(new InputStreamReader(fisl));
		String strLinel;
		int i=0;
		try {
			while ((strLinel = brl.readLine()) != null)   {
				testlabels[i] = Integer.parseInt(strLinel);
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Compare testlabel with predicted labels
		int countAcc = 0;
		for(int p=0;p<7505;p++){
			//			System.out.println(testlabels[p]+"--"+maxResultProbs[p]);
			if(testlabels[p] == maxResultProbs[p])
				countAcc++;
		}
		// Calculating Accuracy
		System.out.println("\nNumber of correctly Classified Documents: "+countAcc);
		System.out.println("Total number of documents: "+7505);
		float accuracyPercentage = ((float)countAcc*100/7505);
		System.out.println("Percantage of accuracy is "+accuracyPercentage+"%");
		confusionMatrix(testlabels, maxResultProbs);
	}

	// confusionMatrix
	void confusionMatrix(int[] testlabels, int[] maxResultProbs){

		int[][] confMat = new int[20][20];
		for(int i=0;i<7505;i++){
			if(testlabels[i] == maxResultProbs[i]){
				confMat[testlabels[i]-1][maxResultProbs[i]-1] = confMat[testlabels[i]-1][maxResultProbs[i]-1]+1;
			}else{
				confMat[testlabels[i]-1][maxResultProbs[i]-1] = confMat[testlabels[i]-1][maxResultProbs[i]-1]+1;
			}
		}

		System.out.println("Confusion Matrix is: ");
		for(int i=0;i<20;i++){
			for(int j=0;j<20;j++){
				System.out.print(confMat[i][j]+"	");
			}
			System.out.println("\n");
		}

		HashMap<Integer, Double> newsGroupAccuracies = new  HashMap<>();
		for(int i=0;i<20;i++){
			int correctlyClassified =0, misClassified =0;
			for(int j=0;j<20;j++){
				if(i==j){
					correctlyClassified = confMat[i][j];
				}else{
					misClassified = misClassified+confMat[i][j];
				}
			}
			double acc;
			acc = (double)correctlyClassified/(correctlyClassified+misClassified)*100;
			newsGroupAccuracies.put(i+1, acc);
		}
	}
}